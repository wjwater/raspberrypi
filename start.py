import RPi.GPIO as GPIO
import time
import os

#adjust for where your switch is connected
switch = 23
GPIO.setmode(GPIO.BCM)
GPIO.setup(switch,GPIO.IN)

while True:
  #assuming the script to call is long enough we can ignore bouncing
  if (GPIO.input(switch) == False):
    #this is the script that will be called (as root)
    os.system("python /home/pi/soundButton.py")
