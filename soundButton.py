#!/usr/bin/env python
#http://www.soundjay.com/
 
from time import sleep
import os
import RPi.GPIO as GPIO
import random
 
LED = 18
BUTTON = 23

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON, GPIO.IN)
GPIO.setup(LED, GPIO.OUT)
 
while True:
        if ( GPIO.input(BUTTON) == False ):
		GPIO.output(LED, True)
		randomNr = random.randint(1,7)
		fileName = 'sound' + str(randomNr) + '.mp3'
		os.system('mpg321 ' + fileName)
		GPIO.output(LED, False)
        sleep(0.1);
