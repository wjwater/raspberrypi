#!/usr/bin/env python
 
from time import sleep
import os
import RPi.GPIO as GPIO
 
LED = 18
BUTTON = 23

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON, GPIO.IN)
GPIO.setup(LED, GPIO.OUT)
 
while True:
        if ( GPIO.input(BUTTON) == False ):
		GPIO.output(LED, True)
		sleep(1)
		GPIO.output(LED, False)
        sleep(0.1);
