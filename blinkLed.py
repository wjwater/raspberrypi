#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

DEBUG = 1

GPIO.setmode(GPIO.BCM)
LED = 18
GPIO.setup(LED, GPIO.OUT)

def Blink(numTimes, speed):
	for i in range(0, 10):
		print "blink " + str(i+1)
		GPIO.output(LED, True)
		time.sleep(1)
		GPIO.output(LED, False)
		time.sleep(1)
	print "done"
	GPIO.cleanup()

Blink(10, 11)
